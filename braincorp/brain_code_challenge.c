/* brain_code_challenge.c
 * Created Oct 19, 2017 by Jason Murray
 */

#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <assert.h>
#include <inttypes.h>

/* message types */
/* some prefer static const <type> for this type of thing -- it can be better for debug as actually a symbol and not a
 * literal. I'll follow what is being done in an existing code base */
#define DISPLAY_MSG 0x34
#define MOTOR_MSG   0x80

/* message offsets and sizes */
#define MSG_ID_INDEX    0
#define MSG_TYPE_INDEX  1
#define MSG_LEN_INDEX   2
#define MSG_LEN_SIZE    2
#define DISPLAY_MSG_DATA_INDEX 4
#define MOTOR_MSG_DATA_SIZE 4
#define MOTOR_MSG_FB_DATA_INDEX 4
#define MOTOR_MSG_LR_DATA_INDEX 8

/* return values */
#define OK 0
#define BAD_ARG -1
#define UNKNOWN_MSG_TYPE -2

/* more efficient to use the memory from original messages array, but if a separate buffer is required for data array
 * , uncomment the line below to instead statically allocate memory for the display message */
/* #define USE_GLOBAL_DATA_ARRAY */
#ifdef USE_GLOBAL_DATA_ARRAY
#define MAX_DATA_SIZE 65536
/* assuming full size display message in a separate buffer needs to be supported, statically allocate */
char g_data[MAX_DATA_SIZE] = {'\0'};
#endif

#define DEBUG 1
#ifdef DEBUG
#define dbg_printf(...) printf(__VA_ARGS__)
#else
#define dbg_printf(...)
#endif

/* normally I would create a header with my declarations, and include, but this seemed easier to look at given size of problem */
int update_motor(float forward_back, float left_right);
int display_message(const char *msg, uint16_t size);
int dispatch_message(const uint8_t *msg);

int update_motor(float forward_back, float left_right) {
    dbg_printf("forward_back: %f, left_right: %f\n", forward_back, left_right);
    return OK;
}

int display_message(const char *msg, uint16_t size) {
    if (NULL == msg) {
        return BAD_ARG;
    }
    dbg_printf("msg: %.*s\n", size, msg);
    return OK;
}

/* dispatches motor and display messages to their respective handlers
 * INPUT: little endian message with format:
 *  packet_id (byte, position 0)
 *  message_type (byte, position 1)
 *  message_length (2 bytes, positions 2 to 3)
 *  message_data (positions 4 to 4+message_length)
 * RETURNS: OK, BAD_ARG, UNKNOWN_MSG_TYPE
 */
int dispatch_message(const uint8_t *msg) {
    if (NULL == msg) {
        dbg_printf("bad argument\n");
        return BAD_ARG;
    }

    uint8_t msg_id = msg[MSG_ID_INDEX];
    dbg_printf("processing message %u\n", msg_id);
    uint8_t msg_type = msg[MSG_TYPE_INDEX];
    uint16_t msg_size = 0;
    switch (msg_type) {
        /* depending on number of message types, may be better to delegate parse code to helpers, but problem small
         * enough to do parsing here */
        case DISPLAY_MSG:
            dbg_printf("display msg\n");
            /* seems irrelevant, but present for motor messages.  Only parse for display messages */
            memcpy(&msg_size, &msg[MSG_LEN_INDEX], MSG_LEN_SIZE);
            dbg_printf("message size is %u\n", msg_size);
#ifdef USE_GLOBAL_DATA_ARRAY
        memcpy((void*)g_data, &msg[DISPLAY_MSG_DATA_INDEX], msg_size);
        const char *display_msg = g_data;
#else
            const char *display_msg = (const char *) &msg[DISPLAY_MSG_DATA_INDEX];
#endif
            display_message(display_msg, msg_size);
            break;
        case MOTOR_MSG:
            dbg_printf("motor msg\n");
            float forward_back = 0;
            float left_right = 0;
            memcpy(&forward_back, &msg[MOTOR_MSG_FB_DATA_INDEX], MOTOR_MSG_DATA_SIZE);
            memcpy(&left_right, &msg[MOTOR_MSG_LR_DATA_INDEX], MOTOR_MSG_DATA_SIZE);
            update_motor(forward_back, left_right);
            break;
        default:
            dbg_printf("unknown message type\n");
            return UNKNOWN_MSG_TYPE;
    }
    return OK;
}

int main() {
    /* assumptions about the platform */
    assert(CHAR_BIT * sizeof(float) == 32);    /* float not always 32 bits */
    assert(CHAR_BIT * sizeof(char) == 8);      /* char always a byte, but byte not always 8 bits */

    /* tests -- I'd normally use some framework */
    uint8_t display_packet[] = {0x1, 0x34, 0x05, 0x0, 0x48, 0x65, 0x6c, 0x6c, 0x6f};
    uint8_t motor_packet[] = {0x2, 0x80, 0x08, 0x0, 0x0, 0x0, 0x80, 0x3f, 0x0, 0x0, 0x0, 0xbf};
    uint8_t unknown_packet[] = {0x3, 0xFF, 0x08, 0x0, 0x0, 0x0, 0x80, 0x3f, 0x0, 0x0, 0x0, 0xbf};
    assert(dispatch_message(display_packet) == OK);
    printf("\n");
    assert(dispatch_message(motor_packet) == OK);
    printf("\n");
    assert(dispatch_message(unknown_packet) == UNKNOWN_MSG_TYPE);
    printf("\n");
    assert(dispatch_message(NULL) == BAD_ARG);
    printf("\n");
    printf("done\n");

    return OK;
}




